package main

import (
	"flag"
	"fmt"
	"log"

	"gitlab.com/MrGunflame/gw2api"
)

var today = flag.Bool("today", false, "look at today instead of tomorrow")

func main() {
	flag.Parse()

	api := gw2api.New()

	f := api.TomorrowDailyAchievements
	if *today {
		log.Println("here")
		f = api.DailyAchievements
	}

	dach, err := f()
	if err != nil {
		log.Fatal(err)
	}

	var ids []int
	idmap := make(map[int]*gw2api.DailyAchievement)
	for _, v := range dach["pve"] {
		ids = append(ids, v.ID)
		idmap[v.ID] = v
	}
	// log.Println(ids)
	ach, err := api.Achievements(ids...)
	if err != nil {
		log.Fatal(err)
	}

	ids = []int{}
	for _, v := range dach["wvw"] {
		ids = append(ids, v.ID)
		idmap[v.ID] = v
	}
	// log.Println(ids)
	ach2, err := api.Achievements(ids...)
	if err != nil {
		log.Fatal(err)
	}

	ach = append(ach, ach2...)

	for _, v := range ach {
		fmt.Println(v.Name, "-", v.Description, v.Flags, idmap[v.ID].RequiredAccess)
		fmt.Println("\t" + v.Requirement)
	}
}
